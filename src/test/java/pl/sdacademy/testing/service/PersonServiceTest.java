package pl.sdacademy.testing.service;

import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.Test;
import pl.sdacademy.testing.entity.EmailAddress;
import pl.sdacademy.testing.entity.Person;
import pl.sdacademy.testing.entity.PeselNumber;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PersonServiceTest {
	@Test
	void testFormatPerson() {
		//given
		final Person person = mock(Person.class);
		when(person.peselNumber()).thenReturn(new PeselNumber("12345678901"));
		when(person.emailAddress()).thenReturn(new EmailAddress("abc@gmail.com"));
		when(person.id()).thenReturn(1);
		when(person.firstName()).thenReturn("Jan");
		when(person.lastName()).thenReturn("Kowalski");

		//when
		final String result = PersonService.formatPerson(person);

		//then
		assertThat(result).isEqualTo("#1. Jan Kowalski, 12345678901 (abc@gmail.com)");
	}

	@Test
	void testGetPeopleEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);

		// when
		final List<Person> result = personService.getPeople();

		// then
		assertThat(result).isEmpty();
	}

	@Test
	void testGetPeopleNonEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);
		final Person person = new Person(1, "Jan", "Kowalski", new EmailAddress("a@a.pl"), new PeselNumber("12312312312"));
		personService.addPerson(person);

		// when
		final List<Person> result = personService.getPeople();

		// then
		assertThat(result).containsExactly(person);
	}

	@Test
	void testGetNextAvailableId() {
		// given
		final IdentificationService identificationService = mock(IdentificationService.class);
		when(identificationService.next()).thenReturn(200);
		final PersonService personService = new PersonService(identificationService, null);

		// when
		final int result = personService.getNextAvailableId();

		// then
		assertThat(result).isEqualTo(200);
	}

	@Test
	void testCreatePerson() {
		// given
		final IdentificationService identificationService = mock(IdentificationService.class);
		when(identificationService.next()).thenReturn(100);
		final PersonService personService = new PersonService(identificationService, null);

		// when
		final Person person = personService.createPerson("Jan", "Kowalski", "email@email.com", "12312312312");

		// then
		final Person expected = new Person(100, "Jan", "Kowalski", new EmailAddress("email@email.com"), new PeselNumber("12312312312"));
		assertThat(person).isEqualTo(expected);
	}

	@Test
	void getPersonByIdWithEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);

		// when
		final Person person = personService.getPerson(0);

		// then
		assertThat(person).isNull();
	}

	@Test
	void getPersonByIdWithNonEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);
		final int id = 1;
		final Person person = new Person(id, "Jan", "Kowalski", new EmailAddress("a@a.pl"), new PeselNumber("12312312312"));
		personService.addPerson(person);

		// when
		final Person result = personService.getPerson(id);

		// then
		assertThat(result).isSameAs(person);
	}

	@Test
	void getPersonByPeselNumberWithEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);

		// when
		final Person person = personService.getPerson(new PeselNumber("12312312312"));

		// then
		assertThat(person).isNull();
	}

	@Test
	void getPersonByPeselNumberWithNonEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);
		final PeselNumber peselNumber = new PeselNumber("12312312312");
		final Person person = new Person(1, "Jan", "Kowalski", new EmailAddress("a@a.pl"), peselNumber);
		personService.addPerson(person);

		// when
		final Person result = personService.getPerson(peselNumber);

		// then
		assertThat(result).isSameAs(person);
	}

	@Test
	void getPersonByEmailAddressWithEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);

		// when
		final Person person = personService.getPerson(new EmailAddress("a@a.pl"));

		// then
		assertThat(person).isNull();
	}

	@Test
	void getPersonByEmailAddressWithNonEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);
		final EmailAddress emailAddress = new EmailAddress("a@a.pl");
		final Person person = new Person(1, "Jan", "Kowalski", emailAddress, new PeselNumber("12312312312"));
		personService.addPerson(person);

		// when
		final Person result = personService.getPerson(emailAddress);

		// then
		assertThat(result).isSameAs(person);
	}

	@Test
	void addPersonWithCollidingId() {
		// given
		final PersonService personService = new PersonService(null, null);
		final Person person = new Person(1, "Jan", "Kowalski", new EmailAddress("a@a.pl"), new PeselNumber("12312312312"));
		personService.addPerson(person);

		// when
		final ThrowableAssert.ThrowingCallable callable = () ->
				personService.addPerson(new Person(1, "Jakub", "Nowak", new EmailAddress("b@b.pl"), new PeselNumber("12345678901")));

		// then
		assertThatThrownBy(callable)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Person with this id already exists!");
	}

	@Test
	void addPersonWithCollidingEmailAddress() {
		// given
		final PersonService personService = new PersonService(null, null);
		final Person person = new Person(1, "Jan", "Kowalski", new EmailAddress("a@a.pl"), new PeselNumber("12312312312"));
		personService.addPerson(person);

		// when
		final ThrowableAssert.ThrowingCallable callable = () ->
				personService.addPerson(new Person(2, "Jakub", "Nowak", new EmailAddress("a@a.pl"), new PeselNumber("12345678901")));

		// then
		assertThatThrownBy(callable)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Person with this email address already exists!");
	}

	@Test
	void addPersonWithCollidingPeselNumber() {
		// given
		final PersonService personService = new PersonService(null, null);
		final Person person = new Person(1, "Jan", "Kowalski", new EmailAddress("a@a.pl"), new PeselNumber("12312312312"));
		personService.addPerson(person);

		// when
		final ThrowableAssert.ThrowingCallable callable = () ->
				personService.addPerson(new Person(2, "Jakub", "Nowak", new EmailAddress("b@b.pl"), new PeselNumber("12312312312")));

		// then
		assertThatThrownBy(callable)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Person with this pesel number already exists!");
	}

	@Test
	void addPersonToEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);
		final Person person = new Person(1, "Jan", "Kowalski", new EmailAddress("a@a.pl"), new PeselNumber("12312312312"));

		// when
		personService.addPerson(person);

		// then
		assertThat(personService.getPeople()).containsExactly(person);
	}

	@Test
	void addPersonToNonEmptyList() {
		// given
		final PersonService personService = new PersonService(null, null);
		final Person person1 = new Person(1, "Jan", "Kowalski", new EmailAddress("a@a.pl"), new PeselNumber("12312312312"));
		final Person person2 = new Person(2, "Jakub", "Nowak", new EmailAddress("b@b.pl"), new PeselNumber("12345678901"));
		personService.addPerson(person1);

		// when
		personService.addPerson(person2);

		// then
		assertThat(personService.getPeople()).containsExactlyInAnyOrder(person1, person2);
	}

	@Test
	void saveWithOnePerson() {
		// given
		final FileService fileService = mock(FileService.class);
		final PersonService personService = new PersonService(null, fileService);
		final Person person = new Person(1, "Jan", "Kowalski", new EmailAddress("a@a.pl"), new PeselNumber("12312312312"));
		personService.addPerson(person);

		// when
		personService.save();

		// then
		verify(fileService).save(person);
	}
}
