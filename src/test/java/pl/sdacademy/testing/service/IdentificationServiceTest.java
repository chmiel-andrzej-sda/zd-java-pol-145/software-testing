package pl.sdacademy.testing.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class IdentificationServiceTest {
	@Test
	void nextTwoValuesTest() {
		// given
		final IdentificationService identificationService = new IdentificationService();

		//when
		final int result1 = identificationService.next();
		final int result2 = identificationService.next();

		// then
		assertThat(result1).isZero();
		assertThat(result2).isEqualTo(1);
	}
}
