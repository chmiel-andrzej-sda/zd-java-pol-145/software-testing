package pl.sdacademy.testing.entity;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

class PeselNumberTest {
	static Collection<Object[]> getData() {
		final Object[][] objects = {
				{"12312312312", true},
				{"22222222222", true},
				{"12345678901", true},
				{"", false},
				{"123456789", false},
				{"abc", false}
		};
		return Arrays.asList(objects);
	}

	@MethodSource("getData")
	@ParameterizedTest
	void test(final String number, final boolean expected) {
		// when
		final PeselNumber peselNumber = new PeselNumber(number);

		// then
		if (expected) {
			assertThat(peselNumber.value()).isEqualTo(number);
		} else {
			assertThat(peselNumber.value()).isNull();
		}
	}
}
