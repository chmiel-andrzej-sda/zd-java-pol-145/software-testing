package pl.sdacademy.testing.entity;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

class EmailAddressTest {
	static Collection<Object[]> getData() {
		final Object[][] objects = {
				{"a@a.pl", "a@a.pl"},
				{"@", "@"},
				{"aaaaaaaaa@aaaaaaa.pl", "aaaaaaaaa@aaaaaaa.pl"},
				{"a.pl", null},
				{"", null},
				{"xyz", null}
		};
		return Arrays.asList(objects);
	}

	@MethodSource("getData")
	@ParameterizedTest
	void test(final String email, final String expected) {
		// when
		final EmailAddress emailAddress = new EmailAddress(email);

		// then
		assertThat(emailAddress.value()).isEqualTo(expected);
	}
}
