package pl.sdacademy.testing.service;

import pl.sdacademy.testing.entity.Person;

import java.io.IOException;
import java.io.PrintWriter;

public class FileService {
	public void save(final Person person) {
		final String fileName = String.format("%s-%s.txt", person.firstName(), person.lastName());
		try (final PrintWriter printWriter = new PrintWriter(fileName)) {
			printWriter.println(person);
		} catch (final IOException exception) {
			exception.printStackTrace();
		}
	}
}
